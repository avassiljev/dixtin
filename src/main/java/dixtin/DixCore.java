package dixtin;

import java.lang.reflect.Modifier;
import java.util.function.BooleanSupplier;

import dixtin.exception.BindingNotFoundException;
import dixtin.exception.DixException;

public class DixCore
{
	private Mapping m_map;
	private ObjectFactory m_objectFactory;
	private Class<?> m_setKeyType = null;
	private Class<?> m_setBoundType = null;
	
	private void ensureNotNull(Class<?> type)
	{
		if(type == null)
		{
			throw new NullPointerException();
		}
	}
	private void ensureConcreteClass(Class<?> type)
	{
		if(type.isInterface() || Modifier.isAbstract(type.getModifiers()))
		{
			throw new IllegalArgumentException("Must be bound to a concrete class");
		}
	}
	
	private void ensureNotConcreteClass(Class<?> type)
	{
		if(!type.isInterface() && !Modifier.isAbstract(type.getModifiers()))
		{
			throw new IllegalArgumentException("Key type must be interface or abstract class");
		}
	}
	
	public DixCore()
	{
		m_map = new Mapping();
		m_objectFactory = new ObjectFactory(this, m_map);
	}
	
	public DixCore bind(Class<?> type)
	{
		ensureNotNull(type);
		ensureNotConcreteClass(type);
		
		m_setKeyType = type;
		m_setBoundType = null;
		return this;
	}
	
	public DixCore to(Class<?> type) throws DixException
	{
		ensureNotNull(type);
		ensureConcreteClass(type);
		
		if(m_setBoundType != null)
		{
			throw new DixException("bound class alredy set");
		}
		m_setBoundType = type;
		m_map.add(m_setKeyType, m_setBoundType);
		return this;
	}
	
	public <T> DixCore withPropertyValue(String propertyName, T value)
	{
		m_map.get(m_setKeyType, m_setBoundType).addPropertyValue(propertyName, value);
		return this;
	}
	
	public <T> DixCore withPropertyInject(String propertyName)
	{
		m_map.get(m_setKeyType, m_setBoundType).addInjectPropertyName(propertyName);
		return this;
	}
	
	public <T> DixCore withConstructorParameter(int position, T value)
	{
		if(position < 1)
		{
			throw new IllegalArgumentException("position starts with 1");
		}
		m_map.get(m_setKeyType, m_setBoundType).addConstructorParameter(position, value);
		return this;
	}
	
	public <T> DixCore withConstructorParameter(int position, Class<?> type, T value)
	{
		if(position < 1)
		{
			throw new IllegalArgumentException("position starts with 1");
		}
		m_map.get(m_setKeyType, m_setBoundType).addConstructorParameter(position, type, value);
		return this;
	}
	
	public DixCore when(BooleanSupplier func)
	{
		m_map.get(m_setKeyType, m_setBoundType).addConditionWhen(func);
		return this;
	}
	
	public DixCore whenInjectedInto(Class<?> type)
	{
		m_map.get(m_setKeyType, m_setBoundType).addConditionInjectedInto(type);
		return this;
	}
	
	/**
	 * Returns the newly created object of specified type.
	 * 
	 * @param type
	 * @return
	 * @throws DixException
	 * @throws ReflectiveOperationException
	 */
	public <T> T get(Class<?> type)
			throws DixException, ReflectiveOperationException
	{
		return get(type, null);
	}
	
	protected <T> T get(Class<?> type, Class<?> injectedInto)
			throws DixException, ReflectiveOperationException
	{
		Binding binding = m_map.findBinding(type, injectedInto);
		if(binding == null)
		{
			throw new BindingNotFoundException();
		}
		
		return m_objectFactory.<T>create(binding);
	}
}
