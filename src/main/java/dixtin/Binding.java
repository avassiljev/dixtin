package dixtin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BooleanSupplier;

/**
 * Defines a binding for a bound type, along with conditions required for that binding
 *
 */
class Binding
{
	private Class<?> m_boundClass;
	private Map<String, Object> m_propertyValues;
	// list of property names which need to be injected. Defined by .withPropertyInject(String)
	private List<String> m_injectProperties;
	// list of conditions defined by .whenInjectedInto(Class<?>)
	private Set<Class<?>> m_condInjectedInto;
	// list of conditions defined by .when(BooleanSupplier)
	private List<BooleanSupplier> m_condWhen;
	// parameters to be used in constructor, indexed by position
	private Map<Integer, MethodParam<?>> m_ctorParams;
	
	private int m_ctorPosition = 0;
	private int m_minCtorCount = 0;
	
	private <K, V> Map<K, V> initMap(Map<K, V> map)
	{
		if(map == null)
		{
			return new HashMap<>();
		}
		return map;
	}
	
	private <T> List<T> initList(List<T> list)
	{
		if(list == null)
		{
			return new ArrayList<>();
		}
		return list;
	}
	
	private <T> Set<T> initSet(Set<T> set)
	{
		if(set == null)
		{
			return new HashSet<>();
		}
		return set;
	}

	Binding(Class<?> boundClass)
	{
		m_boundClass = boundClass;
	}
	
	Class<?> getBoundClass()
	{		
		return m_boundClass;
	}
	
	// -----------------------------------------------------------------------
	
	void addPropertyValue(String propertyName, Object value)
	{
		m_propertyValues = initMap(m_propertyValues);
		
		if(!m_propertyValues.containsKey(propertyName))
		{
			m_propertyValues.put(propertyName, value);
		}
	}
	
	Map<String, Object> getPropertyValues()
	{
		return m_propertyValues;
	}

	// -----------------------------------------------------------------------
	
	void addInjectPropertyName(String propertyName)
	{
		m_injectProperties = initList(m_injectProperties);
		m_injectProperties.add(propertyName);
	}
	
	List<String> getInjectPropertyNames()
	{
		return m_injectProperties;
	}
	
	// -----------------------------------------------------------------------
	
	void addConditionInjectedInto(Class<?> type)
	{
		m_condInjectedInto = initSet(m_condInjectedInto);
		m_condInjectedInto.add(type);
	}
	
	Set<Class<?>> getConditionsInjectedInto()
	{
		return m_condInjectedInto;
	}
	
	// -----------------------------------------------------------------------
	
	void addConditionWhen(BooleanSupplier func)
	{
		m_condWhen = initList(m_condWhen);
		m_condWhen.add(func);
	}
	
	List<BooleanSupplier> getConditionsWhen()
	{
		return m_condWhen;
	}
	
	// -----------------------------------------------------------------------
	
	boolean hasConditions()
	{
		return (m_condInjectedInto != null && !m_condInjectedInto.isEmpty()) ||
				(m_condWhen != null && !m_condWhen.isEmpty());
	}
	
	// -----------------------------------------------------------------------
	
	int getMinCtorParamCount()
	{
		return m_minCtorCount;
	}
	
	<T> void addConstructorParameter(int position, Class<?> type, T value)
	{
		m_ctorParams = initMap(m_ctorParams);
		
		if(position == 0)
		{
			position = ++m_ctorPosition;
		}
		
		if(m_ctorParams.containsKey(position))
		{
			throw new IllegalArgumentException("Parameter is already defined on that position");
		}
		
		m_ctorParams.put(position, new MethodParam<>(type, value));
		m_minCtorCount = Math.max(m_minCtorCount, position);
	}
	
	<T> void addConstructorParameter(int position, T value)
	{
		if(value == null)
		{
			addConstructorParameter(position, Object.class, value);
		}
		else
		{
			addConstructorParameter(position, value.getClass(), value);
		}
	}
	
	<T> void addConstructorParameter(T value)
	{
		addConstructorParameter(0, value);
	}
	
	<T> void addConstructorParameter(Class<?> type, T value)
	{
		addConstructorParameter(0, type, value);
	}
	
	int getConstructorParameterCount()
	{
		return m_ctorParams == null ? 0 : m_ctorParams.size();
	}
	
	Class<?> getConstructorParameterType(int position)
	{
		if(m_ctorParams == null)
		{
			return null;
		}
		MethodParam<?> param = m_ctorParams.get(position);
		if(param == null)
		{
			return null;
		}
		return param.getType();
	}
	
	Object getConstructorParameterValue(int position)
	{
		if(m_ctorParams == null)
		{
			return null;
		}
		return m_ctorParams.get(position).getValue();
		
	}
	boolean isConstructorParameterAtPosition(int position)
	{
		if(m_ctorParams == null)
		{
			return false;
		}
		return m_ctorParams.containsKey(position);
	}
	
}
