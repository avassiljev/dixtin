package dixtin;

import java.lang.reflect.Constructor;
import dixtin.exception.ConstructorNotFoundException;

class ConstructorSelector
{
	private boolean isMatched(Constructor<?> ctor, Binding binding, Mapping map)
	{
		if(ctor.getParameterCount() == 0 && binding.getConstructorParameterCount() > 0)
		{
			return false;
		}
		if(ctor.getParameterCount() < binding.getMinCtorParamCount())
		{
			return false;
		}
		
		Class<?>[] paramTypes = ctor.getParameterTypes();
		for(int i = 0; i < paramTypes.length; i++)
		{
			boolean bFoundBinding = (map.findBinding(paramTypes[i], binding.getBoundClass()) != null);
			Class<?> bindParamType = binding.getConstructorParameterType(i + 1);
			boolean bFoundParam = bindParamType != null && paramTypes[i].equals(bindParamType);
			
			if(!bFoundBinding && !bFoundParam)
			{
				return false;
			}
		}
		return true;
	}
	
	private Constructor<?> findMatchedConstructor(Binding binding, Mapping map)
	{
		Class<?> boundClass = binding.getBoundClass();
		Constructor<?>[] ctors = boundClass.getConstructors();
		
		for(Constructor<?> ctor : ctors)
		{
			if(isMatched(ctor, binding, map))
			{
				return ctor;
			}
		}
		return null;
	}
	
	
	
	Constructor<?> findConstructor(Binding binding, Mapping map)
			throws ConstructorNotFoundException
	{
	
		Constructor<?> ctor = findMatchedConstructor(binding, map);
		if(ctor == null)
		{
			throw new ConstructorNotFoundException();
		}
		return ctor;
	}
}
