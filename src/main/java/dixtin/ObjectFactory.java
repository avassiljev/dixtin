package dixtin;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import dixtin.exception.DixException;

class ObjectFactory
{
	private DixCore m_core;
	private Mapping m_map;
	private ConstructorSelector m_ctorSelector;
	
	ObjectFactory(DixCore core, Mapping map)
	{
		m_core = core;
		m_map = map;
		m_ctorSelector = new ConstructorSelector();
	}
	
	<T> T create(Binding binding)
		throws ReflectiveOperationException, DixException
	{
		Constructor<?> ctor = m_ctorSelector.findConstructor(binding, m_map);
		T object = (T)ctor.newInstance(getConstructorParameters(ctor, binding));
		
		Method[] arrMethods = object.getClass().getMethods();
		Set<Method> listMethods = new HashSet<>();
		for(Method method : arrMethods)
		{
			if(method.getParameterCount() > 0)
			{
				listMethods.add(method);
			}
		}
		
		listMethods = setPropertyValues(object, binding, listMethods);
		listMethods = injectNamedProperties(object, binding, listMethods);
		
		return object;
	}
	
	private Object[] getConstructorParameters(Constructor<?> ctor, Binding binding)
			throws ReflectiveOperationException, DixException
	{
		Object[] paramValues = new Object[ctor.getParameterCount()];
		
		Class<?>[] paramTypes = ctor.getParameterTypes();
		for(int i = 0; i < paramTypes.length; i++)
		{
			if(binding.isConstructorParameterAtPosition(i + 1))
			{
				paramValues[i] = binding.getConstructorParameterValue(i + 1);
			}
			else
			{
				paramValues[i] = m_core.get(paramTypes[i], binding.getBoundClass());
			}
		}
		return paramValues;
	}
	
	private boolean isSetPropertyMethod(Method method, String propertyName)
	{
		if(method.getParameterCount() == 1)
		{
			return method.getName().equalsIgnoreCase("set" + propertyName) ||
					method.getName().equalsIgnoreCase("set_" + propertyName);
		}
		return false;
	}
	
	/**
	 * 
	 * @param object
	 * @param binding
	 * @param listMethods
	 * @return list of methods which were not invoked
	 * @throws DixException
	 * @throws ReflectiveOperationException
	 */
	private <T> Set<Method> injectNamedProperties(T object, Binding binding, Set<Method> listMethods)
			throws DixException, ReflectiveOperationException
	{
		if(binding.getInjectPropertyNames() != null)
		{	
			for(String propName : binding.getInjectPropertyNames())
			{
				for(Method method : listMethods)
				{
					if(isSetPropertyMethod(method, propName))
					{
						Object[] paramValues = new Object[method.getParameterCount()];
						Class<?>[] paramTypes = method.getParameterTypes();
						for(int j = 0; j < paramValues.length; j++)
						{
							paramValues[j] = m_core.get(paramTypes[j], object.getClass());
						}
						method.invoke(object, paramValues);
						listMethods.remove(method);
						break;
					}
				}
			}//for property name
		}
		return listMethods;
	}
	
	/**
	 * Finds public methods starting with "set" or "set_" and having one parameter
	 * and invokes them, passing parameter value defined using withPropertyValue method
	 * Returns list of methods which were not invoked
	 *
	 * @param object
	 * @param binding
	 * @param listMethods
	 * @return list of methods which were not invoked
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private <T> Set<Method> setPropertyValues(T object, Binding binding, Set<Method> listMethods)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		if(binding.getPropertyValues() != null)
		{	
			for(Entry<String, Object> entry : binding.getPropertyValues().entrySet())
			{
				for(Method method : listMethods)
				{
					if(method.getParameterCount() == 1)
					{
						if(isSetPropertyMethod(method, entry.getKey()))
						{
							method.invoke(object, entry.getValue());
							listMethods.remove(method);
							break;
						}
					}
				}//for Method
			}
					
		}
		return listMethods;
	}
}
