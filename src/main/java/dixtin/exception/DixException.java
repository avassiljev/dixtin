package dixtin.exception;

public class DixException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public DixException()
	{
		super();
	}
	
	public DixException(String message)
	{
		super(message);
	}
}
