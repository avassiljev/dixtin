package dixtin;

class MethodParam<T>
{
	private T m_value;
	private Class<?> m_type;
	
	T getValue()
	{
		return m_value;
	}
	
	Class<?> getType()
	{
		return m_type;
	}
	
	
	MethodParam(Class<?> type, T value)
	{
		m_type = type;
		m_value = value;
	}
	
	@Override
	public String toString()
	{
		if(m_type == null) { return ""; }
		return m_type.getName() + " = " + m_value == null ? "" : m_value.toString();
	}
}
