package dixtin;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BooleanSupplier;

/**
 * Keeps a collection of concrete classes which are mapped to key type (interface or abstract class)
 *
 */
class Mapping
{
	private Map<Class<?>, Map<Class<?>, Binding>> map = new HashMap<>();
	
	/**
	 * Add (keyType - boundType) pair.
	 * If it already exists - then do nothing
	 * 
	 * @param keyType
	 * @param boundType
	 */
	void add(Class<?> keyType, Class<?> boundType)
	{
		if(map.containsKey(keyType))
		{
			if(!map.get(keyType).containsKey(boundType))
			{
				map.get(keyType).put(boundType, new Binding(boundType));
			}
		}
		else
		{
			Map<Class<?>, Binding> boundMap = new HashMap<>();
			boundMap.put(boundType, new Binding(boundType));
			map.put(keyType, boundMap);
		}
	}
	
	Map<Class<?>, Map<Class<?>, Binding>> get()
	{
		return map;
	}
	
	Map<Class<?>, Binding> get(Class<?> keyType)
	{
		return map.get(keyType);
	}
	
	Binding get(Class<?> keyType, Class<?> boundType)
	{
		Map<Class<?>, Binding> m = get(keyType);
		if(m != null)
		{
			return m.get(boundType);
		}
		return null;
	}
	
	Binding findBinding(Class<?> keyType, Class<?> condInjectedInto)
	{
		Map<Class<?>, Binding> binds = get(keyType);
		if(binds == null)
		{
			return null;
		}
		
		Binding defaultBinding = null;
		boolean bMatch = true;
		for(Entry<Class<?>, Binding> entry : binds.entrySet())
		{
			bMatch = true;
			Binding bind = entry.getValue();
			if(defaultBinding == null && !bind.hasConditions())
			{
				defaultBinding = bind;
				continue;
			}
			
			// Condition: injected into
			if(bMatch && condInjectedInto != null && bind.getConditionsInjectedInto() != null)
			{
				if(!bind.getConditionsInjectedInto().contains(condInjectedInto))
				{
					bMatch = false;
				}
			}
			
			// Condition: when (true)
			if(bMatch && bind.getConditionsWhen() != null)
			{
				for(BooleanSupplier func : bind.getConditionsWhen())
				{
					if(!func.getAsBoolean())
					{
						bMatch = false;
						break;
					}
				}
			}
			
			
			if(bMatch)
			{
				return bind;
			}
		}
		
		return defaultBinding;
	}
	
}
