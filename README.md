# dixtin
Dependency injection library.

Sample program:
```java
int enemyLevel = 9001;
    	
DixCore d = new DixCore();

d.bind(ITank.class)
	.to(LemanRuss.class)
	.withConstructorParameter(2, "Black");

d.bind(ITank.class)
	.to(Baneblade.class)
	.when(() -> enemyLevel > 9000)
	.withPropertyInject("MainGun")
	.withPropertyValue("Name", "Fury");

d.bind(IGun.class).to(Autocannon.class);
d.bind(IGun.class).to(Lascannon.class).whenInjectedInto(Baneblade.class);

ITank tank = d.get(ITank.class);
tank.shoot();
```
